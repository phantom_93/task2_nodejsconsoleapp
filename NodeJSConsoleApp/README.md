Instructions and installations for this assignment

first of all, from vs code type **npm init**  and modfiy **package.json**
and then install express framework with the command 

### install package
```
npm install express

```

This is a simple Node JS Application that gives the user 3 choices as below: 

Choose an option:  
1. Read package.json
2. Diplay OS info 
3. Start HTTP server 

Here the first command will read json package and this json package has to be in the same folder as this program. 
when the user chooses the second option, it will show the OS information of your computer
when option 3 is chosen, it will start http server and listen to port 3000 and show the user "hello world"


In order to run this application, type in vs code terminal 

**node index.js** 

then type 1, 2, or 3 
only one command per execution is possible for this program. 


