const express = require('express'); 
const app = express();
const port = 3000; 
const hostname = '127.0.0.1';

const fs = require('fs');
const os = require('os'); 
const file = './package.json'

// create a readline.createInterface() method 
const readline = require('readline'); 
const r1 = readline.createInterface({
    input: process.stdin, 
    output: process.stdout
}); 

//r1.prompt(); 

// option string for the user input 
const userInput = `
Choose an option:
1. Read package.json
2. Display OS Info
3. Start HTTP server
Type a number: `; 

r1.question(userInput, (answer)=>{
  switch(answer.trim()){
      case '1':
          readJsonPackage(); 
          break; 
      case '2': 
          displayOSInfo(); 
          break; 
      case '3': 
          startServer(); 
          break; 
      default: 
          console.log(`Invalid Option: ${answer}`);  
          //process.exit(0); 
          break;

  }
  r1.close(); 
  
});



//#region read package.json method
function readJsonPackage(){
    let jsonFile = JSON.parse(fs.readFileSync("./package.json")); 
    console.log(jsonFile); 
}

//#endregion


//#region  Display OS info: 
// system memory 
// free memory
// cpu cores 
// arch 
// platform
// release 
// user 
function displayOSInfo(){
    let osInfo = `
    System/Total Memory: ${os.totalmem()}
    Free Memory: ${os.freemem()}
    CPU Cores: ${os.cpus().length}
    CPU Architecture: ${os.arch()}
    Platform: ${os.platform()}
    Release: ${os.release()}
    User: ${os.userInfo().username}
    `; 
   console.log(osInfo); 
}

//#endregion


//#region StartServer 
/* listening on port 3000 
 when you open localhost:3000 will show you "Hello World"*/
// Default Route for your website 
app.get("/", (req, res) => {
   res.statusCode = 200; 
   res.setHeader('Content-Type', 'Text/plain');
   res.end("Hello World!");
}); 

function startServer(){
    app.listen(port, hostname, ()=>{
        console.log(`Server running at http://${hostname}:${port}`);
    }); 
}
//#endregion




